package agh.qa;

import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

public class PeselValidatorTests {
    @DataProvider
    public Object[][] peselTestDataProvider() {
        return new Object[][]{
                {"44051401359", true},
                {"81012410283", true},
                {"99122410183", true},// wyskoczył błąd -> szukam błędu w programie -> bład w kodowaniu grudnia- poprawiony
                {"20222909969", true},
                {"29323103474", true},
                {"00073117520", true},
                {"85052210281", true},
                {"000731175200", false},
                {"00073117520a", false},
                {"0007311!5201", false},
                {"29323103474", true},
                {"91043101250", false},
                {"20222910284", true},
                {"19222910288", false},// aby spr tylko rok przestepny
                {"19222910280", false},//bledna suma kontrnla
                {"20223110287", false},//31 luty roku przestepnego
                {"34892634285", true},
                {"19222713676", true},
                {"1234", false},
                {"12362245106",false },
                {"12362245105",false }
        };
    }


    @Test(dataProvider = "peselTestDataProvider")
    public void TestPesel(String pesel, boolean shouldBeValid){
        PeselValidator validator = new PeselValidator();

        Assert.assertEquals(shouldBeValid, validator.validate(pesel));
    }
}
