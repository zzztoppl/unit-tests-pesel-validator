package agh.qa;

import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import java.time.LocalDate;

public class PeselExtractorTests {

    @DataProvider
    public Object[][] TestGetSex() {
        return new Object[][]{
                {new byte[]{4,4,0,5,1,4,0,1,3,6,6},"Female" }, //K
                {new byte[]{4,4,0,5,1,4,0,1,3,5,9},"Male" },//M
                {new byte[]{8,5,0,5,2,2,1,0,2,8,1}, "Female"},//K


        };
    }

    @Test(dataProvider = "TestGetSex")
    public void testGetSex( byte[] pesel, String expResult) {
        PeselExtractor peselExtractor = new PeselExtractor(new Pesel(pesel));
        String getSex;
        getSex = peselExtractor.GetSex();
        Assert.assertEquals(getSex, expResult);
    }

    @DataProvider
    public Object[][] TestGetBirthDate() {
        return new Object[][]{
                {new byte[]{4,4,0,5,1,4,0,1,3,5,9},"1944-05-14" },
                { new byte[]{2,9,3,2,3,1,0,3,4,7,4}, "2029-12-31"}
        };
    }

    @Test(dataProvider = "TestGetBirthDate")
    public void testGetBirthDate(byte[] pesel, String expResult) {
        PeselExtractor peselExtractor = new PeselExtractor(new Pesel(pesel));
        LocalDate localDate_ = peselExtractor.GetBirthDate();
        String data = localDate_.toString();
        Assert.assertEquals(data, expResult);

    };


}
