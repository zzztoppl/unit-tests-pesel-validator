package agh.qa;

import org.testng.Assert;
//import org.testng.annotations.BeforeMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

public class PeselTests {
    // private Pesel __pesel;
    //@BeforeMethod
    //public void Set() {
    //    __pesel = new Pesel(new byte[]{6,0,0,1,3,0,0,1,0,7,2});

    // }
    @DataProvider
    public Object[][] peselTestGetBirthYear() {
        return new Object[][]{
                {new Pesel(new byte[]{6, 0, 0, 1, 3, 0, 0, 1, 0, 7, 2}), 1960},
                {new Pesel(new byte[]{0, 0, 2, 1, 0, 1, 0, 6, 3, 9, 0}), 2000},
                {new Pesel(new byte[]{9, 9, 1, 2, 1, 2, 1, 5, 8, 5, 7}), 1999},
                {new Pesel(new byte[]{8, 5, 8, 7, 3, 1, 1, 0, 5, 6, 2}), 1885},
                {new Pesel(new byte[]{8, 5, 4, 5, 2, 2, 1, 0, 2, 8, 3}), 2185},
                {new Pesel(new byte[]{7, 6, 7, 2, 3, 1, 8, 0, 5, 6, 2}), 2276},
        };

    }

    @Test(dataProvider = "peselTestGetBirthYear")
    public void testGetBirthYear(Pesel pesel, int expResult) {
        int nowResult = pesel.getBirthYear();
        Assert.assertEquals(nowResult, expResult);
    }

    @DataProvider
    public Object[][] peselTestGetBirthMonth() {
        return new Object[][]{
                {new Pesel(new byte[]{6, 0, 0, 1, 3, 0, 0, 1, 0, 7, 2}), 1},
                {new Pesel(new byte[]{0, 0, 2, 1, 0, 1, 0, 6, 3, 9, 0}), 1},
                {new Pesel(new byte[]{9, 9, 1, 2, 1, 2, 1, 5, 8, 5, 7}), 12},
                {new Pesel(new byte[]{0, 2, 8, 6, 2, 3, 0, 4, 5, 5, 3}), 6},
                {new Pesel(new byte[]{7, 6, 7, 2, 3, 1, 8, 0, 5, 6, 2}), 12},
                {new Pesel(new byte[]{8, 5, 4, 5, 2, 2, 1, 0, 2, 8, 3}), 5}

        };
    }

    @Test(dataProvider = "peselTestGetBirthMonth")
    public void testGetBirthMonth(Pesel pesel, int expResult) {
        int nowResult = pesel.getBirthMonth();
        Assert.assertEquals(nowResult, expResult);
    }

    @DataProvider
    public Object[][] peselTestGetBirthDay() {
        return new Object[][]{
                {new Pesel(new byte[]{6, 0, 0, 1, 3, 0, 0, 1, 0, 7, 2}), 30},
                {new Pesel(new byte[]{0, 0, 2, 1, 0, 1, 0, 6, 3, 9, 0}), 1},
                {new Pesel(new byte[]{9, 9, 1, 2, 1, 2, 1, 5, 8, 5, 7}), 12},
                {new Pesel(new byte[]{2, 0, 2, 2, 3, 1, 1, 0, 2, 8, 7}), 31}, //31 luty roku przestepnego
                {new Pesel(new byte[]{1, 9, 2, 2, 2, 7, 1, 3, 6, 7, 6}), 27},
                {new Pesel(new byte[]{3, 4, 8, 9, 2, 6, 3, 4, 2, 8, 5}), 26}
        };
    }

    @Test(dataProvider = "peselTestGetBirthDay")
    public void testGetBirthDay(Pesel pesel, int expResult) {
        int nowResult = pesel.getBirthDay();
        Assert.assertEquals(nowResult, expResult);
    }

    @DataProvider
    public Object[][] peselTestCheckSum() {
        return new Object[][]{
                {new Pesel(new byte[]{6, 0, 0, 1, 3, 0, 0, 1, 0, 7, 2}), 2},
                {new Pesel(new byte[]{0, 0, 2, 1, 0, 1, 0, 6, 3, 9, 0}), 0},
                {new Pesel(new byte[]{9, 9, 1, 2, 1, 2, 1, 5, 8, 5, 7}), 7},
                {new Pesel(new byte[]{8, 5, 8, 7, 3, 1, 1, 0, 5, 6, 2}), 2},
                {new Pesel(new byte[]{8, 5, 4, 5, 2, 2, 1, 0, 2, 8, 3}), 3},
                {new Pesel(new byte[]{7, 6, 7, 2, 3, 1, 8, 0, 5, 6, 2}), 2},
        };

    }

    @Test(dataProvider = "peselTestCheckSum")
    public void peselTestCheckSum(Pesel pesel, int expResults) {
        int nowResult = pesel.getCheckSum();
        Assert.assertEquals(nowResult, expResults);
    }

}